import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import AuthorRecipes from "./views/AuthorRecipes.vue";
import BookMarks from "./views/BookMarks.vue";
import NewRecipe from "./views/NewRecipe.vue";
import NewAccount from "./views/NewAccount.vue";
import ConfirmAccount from "./views/ConfirmAccount.vue";
import NotFound from "./views/NotFound.vue";
import MyRecipes from "./views/MyRecipes.vue";
import ViewRecipe from "@/components/ViewRecipe.vue";
import Login from "./views/Login.vue";
import Auth from "@aws-amplify/auth";
import { AmplifyEventBus } from "aws-amplify-vue";
import store from "./store";

Vue.use(Router);

AmplifyEventBus.$on("authState", info => {
  console.log(
    `Here is the auth event that was just emitted by an Amplify component: ${info}`
  );
  if (info === "signedIn") {
    Auth.currentSession()
      .then(data => {
        localStorage.setItem("userId", data.idToken.payload.sub);
        localStorage.setItem(
          "username",
          data.idToken.payload["cognito:username"]
        );
        localStorage.setItem("loggedIn", true);
        store.commit("setLoginStatus");
      })
      .catch(err => console.log(err));
    router.push({ path: "/home" });
  }

  if (info === "signedOut") {
    localStorage.removeItem("userId");
    localStorage.removeItem("loggedIn");
    localStorage.removeItem("username");
    store.commit("setLoginStatus");
    router.push({ path: "/login" });
  }
});

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/newrecipe",
      name: "newrecipe",
      component: NewRecipe,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/register",
      name: "register",
      component: NewAccount,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/confirm_account",
      name: "confirm_account",
      component: ConfirmAccount,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/myrecipes",
      name: "myrecipes",
      component: MyRecipes,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/",
      redirect: "home"
    },
    {
      path: "/not_found",
      name: "not_found",
      component: NotFound,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "*",
      redirect: "/not_found"
    },
    {
      path: "/home",
      name: "home",
      component: Home,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/bookmarks",
      name: "bookmarks",
      component: BookMarks,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/recipe/:id",
      name: "singleRecipe",
      component: ViewRecipe,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/recipe/edit/:id",
      name: "recipeEdit",
      component: NewRecipe,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/recipe/author/:id",
      name: "authorRecipes",
      component: AuthorRecipes,
      meta: {
        requiresAuth: false
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  Auth.currentCredentials({
    bypassCache: false // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
  })
    .then(user => {
      if (requiresAuth) {
        if (user && user.authenticated) {
          next();
        } else {
          if (to.path !== "/login") {
            next("/login");
          }
        }
      } else {
        next();
      }
    })
    .catch(err => {
      if (to.path !== "/login") {
        next("login");
      }
    });
});

export default router;
