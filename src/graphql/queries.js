// eslint-disable
// this is an auto generated file. This will be overwritten

export const getRecipe = `query GetRecipe($id: ID!) {
  getRecipe(id: $id) {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const listRecipes = `query ListRecipes(
  $filter: ModelRecipeFilterInput
  $limit: Int
  $nextToken: String
) {
  listRecipes(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      name
      directions
      description
      ingredients {
        amount
        unit
        description
      }
      images
      tags
      timestamp
      userId
      displayName
      ratings {
        userId
        rating
      }
      rating
      flagged
      public
      forcePrivate
      datatype
    }
    nextToken
  }
}
`;
export const getUser = `query GetUser($id: ID!) {
  getUser(id: $id) {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
export const listUsers = `query ListUsers(
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      username
      email
      tags
      createdAt
      modifiedAt
    }
    nextToken
  }
}
`;
