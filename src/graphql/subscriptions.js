// eslint-disable
// this is an auto generated file. This will be overwritten

export const onCreateRecipe = `subscription OnCreateRecipe {
  onCreateRecipe {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const onUpdateRecipe = `subscription OnUpdateRecipe {
  onUpdateRecipe {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const onDeleteRecipe = `subscription OnDeleteRecipe {
  onDeleteRecipe {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const onCreateUser = `subscription OnCreateUser {
  onCreateUser {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
export const onUpdateUser = `subscription OnUpdateUser {
  onUpdateUser {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
export const onDeleteUser = `subscription OnDeleteUser {
  onDeleteUser {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
