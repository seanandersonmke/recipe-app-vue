// eslint-disable
// this is an auto generated file. This will be overwritten

export const createRecipe = `mutation CreateRecipe($input: CreateRecipeInput!) {
  createRecipe(input: $input) {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const updateRecipe = `mutation UpdateRecipe($input: UpdateRecipeInput!) {
  updateRecipe(input: $input) {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const deleteRecipe = `mutation DeleteRecipe($input: DeleteRecipeInput!) {
  deleteRecipe(input: $input) {
    id
    name
    directions
    description
    ingredients {
      amount
      unit
      description
    }
    images
    tags
    timestamp
    userId
    displayName
    ratings {
      userId
      rating
    }
    rating
    flagged
    public
    forcePrivate
    datatype
  }
}
`;
export const createUser = `mutation CreateUser($input: CreateUserInput!) {
  createUser(input: $input) {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
export const updateUser = `mutation UpdateUser($input: UpdateUserInput!) {
  updateUser(input: $input) {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
export const deleteUser = `mutation DeleteUser($input: DeleteUserInput!) {
  deleteUser(input: $input) {
    id
    username
    email
    tags
    createdAt
    modifiedAt
  }
}
`;
