import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import Amplify, * as AmplifyModules from "aws-amplify";
import { AmplifyPlugin } from "aws-amplify-vue";
import awsmobile from "./aws-exports";
import Auth from "@aws-amplify/auth";
import * as AWS from "aws-sdk";
AWS.config.region = "us-east-1";

Amplify.configure(awsmobile);

Vue.use(AmplifyPlugin, AmplifyModules);

Vue.config.productionTip = false;

Auth.currentCredentials({}).then(user => {
  AWS.config.credentials = user;
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount("#app");
});
