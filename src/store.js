import Vue from "vue";
import Vuex from "vuex";
import * as AWS from "aws-sdk";
import { RECIPESTABLE } from "@/constants/constants";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    recipeStep: 0,
    userTags: [],
    userRatings: [],
    loginStatus: null,
    mainNav: false,
    userData: [],
    showAlert: false,
    alertMessage: ""
  },
  mutations: {
    setUser(state, payload) {
      state.loginStatus = payload.loginStatus;
      state.userData = payload.userData;
    },
    setRating(state, payload) {
      state.userData.ratings = payload;
    },
    setRecipeStep(state, payload) {
      state.recipeStep = payload;
    },
    setBookmarks(state, payload) {
      state.userData.bookmarks = payload;
    },
    setUserTags(state, payload) {
      state.userData = payload;
    },
    setMainNavState(state, payload) {
      if (payload === false) {
        state.mainNav = false;
        return;
      }
      state.mainNav = !state.mainNav;
    },
    setAuthStatus(state, payload) {
      state.authStatus = payload.status;
      state.userId = payload.id;
    },
    showAlert(state, payload) {
      state.alertMessage = payload;
      state.showAlert = true;
      setTimeout(() => {
        state.showAlert = false;
      }, 3000);
    },
    setLoginStatus(state) {
      let loginStatus = localStorage.getItem("loggedIn");

      if (loginStatus === null) {
        localStorage.setItem("loggedIn", false);
        state.userData = [];
      }
      //seems that some browsers return a boolean and some save booleans as strings
      if (loginStatus === true || loginStatus === "true") {
        let params = {
            TableName: RECIPESTABLE,
            AttributesToGet: ["tags", "ratings", "bookmarks", "id"],
            Key: {
              id: localStorage.getItem("userId")
            }
          },
          docClient = new AWS.DynamoDB.DocumentClient(),
          tags = docClient.get(params).promise();
        tags
          .then(data => {
            state.loginStatus = true;
            state.userData = data.Item;
          })
          .catch(err => {});
      } else {
        state.loginStatus = false;
        state.userData = [];
      }
    }
  },
  actions: {
    setLoginStatus({ commit }) {
      let loginStatus = localStorage.getItem("loggedIn");

      if (loginStatus === null) {
        localStorage.setItem("loggedIn", false);
        state.userData = [];
      }
      //seems that some browsers localStorage returns a boolean and some return a boolean as a string
      if (loginStatus === true || loginStatus === "true") {
        let params = {
            TableName: RECIPESTABLE,
            AttributesToGet: ["tags", "ratings", "bookmarks", "id"],
            Key: {
              id: localStorage.getItem("userId")
            }
          },
          docClient = new AWS.DynamoDB.DocumentClient();
        return docClient.get(params).promise();
      } else {
        return false;
      }
    }
  }
});
