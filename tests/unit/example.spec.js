import { expect } from "chai";
const fetch = require("node-fetch");
import { shallowMount, createLocalVue } from "@vue/test-utils";
import { API, graphqlOperation, Auth } from "aws-amplify";
import * as mutations from "@/graphql/mutations";
import NewRecipe from "@/views/NewRecipe";
import Amplify, * as AmplifyModules from "aws-amplify";
import { AmplifyPlugin } from "aws-amplify-vue";
import awsmobile from "@/aws-exports";

const localVue = createLocalVue();

Amplify.configure(awsmobile);

localVue.use(AmplifyPlugin, AmplifyModules);

// describe("HelloWorld.vue", () => {
//   it("renders props.msg when passed", () => {
//     const msg = "new message";
//     const wrapper = shallowMount(HelloWorld, {
//       propsData: { msg }
//     });
//     expect(wrapper.text()).to.include(msg);
//   });
// });

it("saves row to table", async () => {
  const wrapper = shallowMount(NewRecipe);
  const random = () => {
    return Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "");
  };

  await Auth.signIn("shyguy", "Gwar4you#");

  let form = {
    name: random(),
    description: random(),
    ingredients: {
      amount: random(),
      unit: random(),
      description: random()
    },
    directions: [random(), random(), random(), random()],
    images: ["BFV36537_CC2017_2IngredintDough4Ways-FB.jpg", "1126803-lg.jpg"],
    tags: ["pizza", "soup", "dinner"],
    userId: "902e66b7-2024-406a-9946-dea05c970450"
  };

  // let promise = await API.graphql(
  //   graphqlOperation(mutations.createRecipe, { input: form })
  // );
  // wrapper.vm.$nextTick(() => {
  //   expect(promise).resolves;
  //   done();
  // });
});
