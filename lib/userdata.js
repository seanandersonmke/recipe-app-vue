import "core-js/modules/es6.regexp.to-string";
import "core-js/modules/es6.regexp.replace";
import { API, graphqlOperation, Auth } from "aws-amplify"; // require("aws-amplify");
// require('../src/graphql/mutations')

import * as mutations from "@/graphql/mutations";

var random = function random() {
  return Math.random().toString(36).replace(/[^a-z]+/g, "");
};

var form = {
  name: random(),
  description: random(),
  ingredients: {
    amount: random(),
    unit: random(),
    description: random()
  },
  directions: [random(), random(), random(), random()],
  images: ["BFV36537_CC2017_2IngredintDough4Ways-FB.jpg", "1126803-lg.jpg"],
  tags: ["pizza", "soup", "dinner"],
  userId: "902e66b7-2024-406a-9946-dea05c970450"
}; // postData();
// async function postData() {
//   await Auth.signIn("shyguy", "Gwar4you#");
//   await API.graphql(graphqlOperation(mutations.createRecipe, { input: form }));
// }