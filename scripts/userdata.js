const NUMBER_OF_RECIPES = 3; //change to increase/decrease number of recipes created per user

global.fetch = require("node-fetch");

function alert(x) {
  x === "undefined" ? console.log("undefined") : console.log(x);
  return;
}

const AWS = require("aws-sdk"),
  uuid = require("uuid"),
  AmazonCognitoIdentity = require("amazon-cognito-identity-js"),
  CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool,
  credentials = new AWS.SharedIniFileCredentials({ profile: "melvins4u" });

AWS.config.credentials = credentials;
AWS.config.update({ region: "us-east-1" });
const dynamodb = new AWS.DynamoDB.DocumentClient({ apiVersion: "2012-08-10" }),
  randomString = () => {
    return Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "");
  };

function getRandomInt() {
  return Math.floor(Math.random() * 100);
}

function getRandomTag(max) {
  return Math.floor(Math.random() * Math.floor(max) + 1);
}

let poolData = {
    UserPoolId: "us-east-1_EAtoQVHgO", // Your user pool id here
    ClientId: "2r55se49r6n67h74jom0b3ro92" // Your client id here
  },
  userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData),
  attributeList = [],
  // userName = randomString(),
  userName = "tupac",
  dataEmail = {
    Name: "email",
    Value: `${userName}@recipesizzler.com`
  },
  attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail),
  userId;

attributeList.push(attributeEmail);

async function newUser() {
  await userPool.signUp(
    userName,
    "password",
    attributeList,
    null,
    (err, result) => {
      if (err) {
        console.log(err.message || JSON.stringify(err));
        return;
      }
      console.log(result.userSub);
      userId = result.userSub;
      createData();
    }
  );
}

async function createData() {
  for (let i = 0; i < NUMBER_OF_RECIPES; i++) {
    let description = "",
      tagsList = [
        "meat",
        "dairy",
        "spam",
        "muffins",
        "eggs",
        "dinner",
        "breakfast",
        "lunch",
        "squill"
      ],
      tags = [],
      imagesList = [
        "chicken.jpg",
        "chicken2.jpg",
        "cookie.jpg",
        "mmmpizza.jpg",
        "noodles.jpg",
        "pie2.jpg",
        "pizzayum.jpg",
        "soup.jpg",
        "soup2.jpg"
      ],
      images = [];

    for (let i = 0; i < getRandomInt(); i++) {
      description += ` ${randomString()}`;
    }

    for (let i = 0; i < 4; i++) {
      let tagNumber = getRandomTag(tagsList.length - 1),
        randomTag = tagsList[tagNumber];

      if (tags.indexOf(randomTag) === -1) {
        tags.push(randomTag);
      }

      let imgNumber = getRandomTag(imagesList.length - 1),
        randomImg = imagesList[imgNumber];

      if (images.indexOf(randomImg) === -1) {
        images.push(randomImg);
      }
    }

    let id = uuid.v4(),
      form = {
        name: randomString(),
        ingredients: [
          {
            amount: randomString(),
            description: randomString(),
            unit: randomString()
          }
        ],
        description: description,
        directions: [
          randomString(),
          randomString(),
          randomString(),
          randomString()
        ],
        timestamp: new Date().getTime(),
        datatype: "recipe",
        images: images,
        tags: tags,
        displayName: userName,
        userId: userId,
        id: id,
        rating: 0,
        ratings: []
      },
      params = {
        TableName: "Recipe-mgkmav3sezah5bnd6ekhcarvne-recipedev",
        Item: form
      };

    dynamodb.put(params, function(err, data) {
      if (err) console.log(err, err.stack);
      // an error occurred
    });
  }
}

newUser();
